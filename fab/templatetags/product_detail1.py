from django import template
from fab.models import Product, Attribute, AttributeBase, ProductAttribute

register = template.Library()


# @register.simple_tag
# def get_attribute_value(string):
#     # values = Attribute.objects.get(=string)
#     # print('++++++++++++++++++\t\t', values)
#     # return values
#     base = ProductAttribute.objects.get(pk=string)
#     print('++++++++++++++++++\t\t', base.attributes)
#     # for attribute in base.attributes.all():
#         # self.attributes.add(attribute)

@register.simple_tag
def get_recent_products(string):
    recent_products = Product.objects.exclude(pk=string.pk).filter(category=string.category)
    return recent_products

@register.simple_tag
def get_brand_products(string):
    brand_products = Product.objects.exclude(pk=string.pk).filter(brand=string.brand)
    return brand_products

