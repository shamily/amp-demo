from django.contrib import admin
from fab.models import Product, ProductImage, ProductAttribute,AttributeBase,Attribute

# Register your models here.


class ProductImageInline(admin.TabularInline):
    model = ProductImage

class ProductAttributeInline(admin.TabularInline):
    model = ProductAttribute

class ProductAdmin(admin.ModelAdmin):
    inlines = [
        ProductImageInline,
        ProductAttributeInline
    ]

admin.site.register(Product, ProductAdmin)
admin.site.register(AttributeBase)
