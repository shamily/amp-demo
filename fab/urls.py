from django.urls import path, re_path
from fab.views import IndexView, ProductDetail, ProductDetailAMP

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    re_path(r'^product/(?P<slug>[\w-]+)/(?P<pk>\d+)/$', ProductDetail.as_view(), name='detail'),
    re_path(r'^product/amp/(?P<slug>[\w-]+)/(?P<pk>\d+)/$', ProductDetailAMP.as_view(), name='detail-amp'),
]
