import os
import random

from django.db import models

# Create your models here.

def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext

def upload_image_path(instance, filename):
    new_filename = random.randint(1,3910209312)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "products/{new_filename}/{final_filename}".format(
            new_filename=new_filename,
            final_filename=final_filename
            )


class Product(models.Model):
    title           = models.CharField(max_length=120)
    availability    = models.CharField(max_length=120, null=True)
    brand           = models.CharField(max_length=120, null=True)
    slug            = models.SlugField(blank=True, unique=True)
    description     = models.TextField()
    price           = models.DecimalField(decimal_places=2, max_digits=20, default=39.99)
    featured        = models.BooleanField(default=False)
    active          = models.BooleanField(default=True)
    timestamp       = models.DateTimeField(auto_now_add=True)
    main_image      = models.ImageField(upload_to=upload_image_path, null=True, blank=True)
    quantity        = models.IntegerField(default=0, null=True)
    section         = models.IntegerField(choices=((1, "MEN"),
                                        (2, "WOMEN"),
                                        (3,"KIDS")
                                        ),
                                default=1, null=True)
    category        = models.CharField(null=True, max_length=200)


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/product/{slug}/{pk}".format(slug=self.slug, pk=self.pk)

class ProductImage(models.Model):
    image           = models.ImageField(upload_to=upload_image_path, null=True, blank=True)
    product         = models.ForeignKey(Product, on_delete=models.CASCADE,  related_name='photos')


class AttributeBase(models.Model):
    label = models.CharField(max_length=255) # e.g. color, size, shape, etc.

    def __str__(self):
        return self.label

class Attribute(models.Model):
    base = models.ForeignKey(AttributeBase, on_delete=models.CASCADE, related_name='attributes')
    value = models.CharField(max_length=255)  # e.g. red, L, round, etc.
    internal_value = models.CharField(max_length=255, null=True, blank=True) # other values you may need e.g. #ff0000, etc.


class ProductAttribute(Attribute):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='attributes')

