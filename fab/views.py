from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import Http404
from django.views.generic import TemplateView, DetailView
from django.shortcuts import render, get_object_or_404, redirect


from .models import Product

# Create your views here.

# def index(request):
#     # latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     template = loader.get_template('fab/templates/index.html')
#     return HttpResponse(template.render(request))
# /home/sayone/projects/amp_demo/templates/index.html


class IndexView(TemplateView):
   """
     Index View
   """
   template_name = 'index.html'

   def get_context_data(self, *args, **kwargs):
        return {'object': Product.objects.all(), }

class ProductDetail(DetailView):
    model = Product
    template_name = 'product_detail.html'

    def get_context_data(self, **kwargs):
        return {'product': Product.objects.get(pk=self.kwargs['pk'])}

    def get_current_object(self, *args, **kwargs):
        request = self.request
        slug = self.kwargs.get('slug')

        #instance = get_object_or_404(Product, slug=slug, active=True)
        try:
            instance = Product.objects.get(slug=slug, active=True)
        except Product.DoesNotExist:
            raise Http404("Not found..")
        except Product.MultipleObjectsReturned:
            qs = Product.objects.filter(slug=slug, active=True)
            instance = qs.first()
        except:
            raise Http404("Uhhmmm ")
        return instance

class ProductDetailAMP(DetailView):
    model = Product
    template_name = 'detail_amp.html'

    def get_context_data(self, **kwargs):
        return {'product': Product.objects.get(pk=self.kwargs['pk'])}

    def get_current_object(self, *args, **kwargs):
        request = self.request
        slug = self.kwargs.get('slug')
        try:
            instance = Product.objects.get(slug=slug, active=True)
        except Product.DoesNotExist:
            raise Http404("Not found..")
        except Product.MultipleObjectsReturned:
            qs = Product.objects.filter(slug=slug, active=True)
            instance = qs.first()
        except:
            raise Http404("Uhhmmm ")
        return instance

   # return HttpResponse(template.render(request))
   # /home/sayone/projects/amp_demo/fab/templates/index.html